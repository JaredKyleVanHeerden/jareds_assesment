from django.db import models

class Client(models.Model):
    name = models.CharField(max_length=50, blank=False, default='')
    address = models.CharField(max_length=50, blank=False, default='')
    dateCreated = models.DateField(blank=False)
    dateUpdated = models.DateField(blank=False)
    deleted = models.BooleanField()
