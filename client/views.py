from django.shortcuts import render

from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser 
from rest_framework import status
from client.models import Client
from assesment.serializer import ClientSerializer
from rest_framework.decorators import APIView
from rest_framework.response import Response
from rest_framework import permissions
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi


class Clients(APIView):
    permission_classes = [permissions.IsAuthenticated]

    token_param_config = openapi.Parameter('token', in_ = openapi.IN_QUERY, description='Get client by id or all clients if none', type=openapi.TYPE_STRING)
    @swagger_auto_schema(manual_parameters=[token_param_config])
    def get(self, request, pk = None):

        try:
            if pk:
                clients = Client.objects.filter(id = pk)

            else:
                clients = Client.objects.all()
        
            clients_serializer = ClientSerializer(clients, many=True)
            return JsonResponse(clients_serializer.data, safe=False)
            # 'safe=False' for objects serialization
        except Exception:
            return JsonResponse('Opps something went wrong', safe=False)

    token_param_config = openapi.Parameter('token', in_ = openapi.IN_QUERY, description='Put client by id', type=openapi.TYPE_STRING)
    @swagger_auto_schema(manual_parameters=[token_param_config])
    def put(self, request, pk):
        try:
            client =  Client.objects.get(pk = pk)
            serializer = ClientSerializer(client, data = request.data, partial = True)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
            else:
                return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST)
        except Exception:
            return JsonResponse('Opps something went wrong', safe=False)

    token_param_config = openapi.Parameter('token', in_ = openapi.IN_QUERY, description='Post client by id', type=openapi.TYPE_STRING)
    @swagger_auto_schema(manual_parameters=[token_param_config])
    def post(self, request):

        try:
            client_data = JSONParser().parse(request)
            client_serializer = ClientSerializer(data=client_data)
            if client_serializer.is_valid():
                client_serializer.save()
                return JsonResponse(client_serializer.data, status=status.HTTP_201_CREATED, safe=False) 
            else:
                return Response(client_serializer.errors, status = status.HTTP_400_BAD_REQUEST)
        except Exception:
            return JsonResponse('Opps something went wrong', safe=False)
             
    token_param_config = openapi.Parameter('token', in_ = openapi.IN_QUERY, description='Delete client by id', type=openapi.TYPE_STRING)
    @swagger_auto_schema(manual_parameters=[token_param_config])
    def delete(self, request, pk):
        
        try:
            Client.objects.filter(id = pk).update(deleted=True)
            clients_serializer = ClientSerializer(Client.objects.filter(id = pk), many=True)
            return JsonResponse('Deleted successfully', safe=False)
        except Exception:
            return JsonResponse('Opps something went wrong', safe=False)