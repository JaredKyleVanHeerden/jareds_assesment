from django.db import models
from client.models import Client

class Project(models.Model):
    clientId = models.ForeignKey(Client, on_delete=models.CASCADE)
    name = models.CharField(max_length=50, blank=False, default='')
    description = models.CharField(max_length=50, blank=False, default='')
    dateCreated = models.DateField(blank=False)
    dateUpdated = models.DateField(blank=False)
    deleted = models.BooleanField()
 