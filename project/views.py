from django.shortcuts import render
from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser 
from rest_framework import status
from project.models import Project
from assesment.serializer import ProjectSerializer
from rest_framework.decorators import APIView
from rest_framework import permissions
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi
from rest_framework.response import Response



class Projects(APIView):
    permission_classes = [permissions.IsAuthenticated]
    
    token_param_config = openapi.Parameter('token', in_ = openapi.IN_QUERY, description='Get project by id', type=openapi.TYPE_STRING)
    @swagger_auto_schema(manual_parameters=[token_param_config])
    def get(self, request, pk = None):

        try:
            if pk:
                projects = Project.objects.filter(id = pk)

            else:
                projects = Project.objects.all()
        
            projects_serializer = ProjectSerializer(projects, many=True)
            return JsonResponse(projects_serializer.data, safe=False)
            # 'safe=False' for objects serialization
        except Exception:
            return JsonResponse('Opps something went wrong', safe=False)

    token_param_config = openapi.Parameter('token', in_ = openapi.IN_QUERY, description='Put project by id', type=openapi.TYPE_STRING)
    @swagger_auto_schema(manual_parameters=[token_param_config])
    def put(self, request, pk = None):

        try:
            project =  Project.objects.get(pk = pk)
            serializer = ProjectSerializer(project, data = request.data, partial = True)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
            else:
                return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST)
        except Exception:
            return JsonResponse('Opps something went wrong', safe=False)

    token_param_config = openapi.Parameter('token', in_ = openapi.IN_QUERY, description='Post project by id', type=openapi.TYPE_STRING)
    @swagger_auto_schema(manual_parameters=[token_param_config])
    def post(self, request, pk = None):

        try:
            project_data = JSONParser().parse(request)
            project_serializer = ProjectSerializer(data=project_data)
            if project_serializer.is_valid():
                project_serializer.save()
                return JsonResponse(project_serializer.data, status=status.HTTP_201_CREATED, safe=False) 
            else:
                return Response(project_serializer.errors, status = status.HTTP_400_BAD_REQUEST)
        except Exception:
            return JsonResponse('Opps something went wrong', safe=False)
             

    token_param_config = openapi.Parameter('token', in_ = openapi.IN_QUERY, description='Delete project by id', type=openapi.TYPE_STRING)
    @swagger_auto_schema(manual_parameters=[token_param_config])
    def delete(self, request, pk = None):

        try:
            Project.objects.filter(id = pk).update(deleted=True)
            ProjectSerializer(Project.objects.filter(id = pk), many=True)
            return JsonResponse('Deleted successfully', safe=False)
        except Exception:
            return JsonResponse('Opps something went wrong', safe=False)